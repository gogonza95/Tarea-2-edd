class Contact
	attr_accessor :name , :surname , :phone , :address
    def initialize (name, surname, phone, address)
     	@name = name
     	@surname = surname
     	@phone = phone
     	@address = address
    end
end

class Node_Hash
	attr_accessor :contact , :next_node
	def initialize (contact,next_node)
		@contact = contact
		@next_node = next_node
	end
end

class Hash_List
	def initialize 
		@head = nil
	end


    def empty?
        @head.nil?
    end


	def insert contact
    	return unless contact.is_a? Contact
        if empty?
        	n = Node_Hash.new(contact,nil)
            @head = n
            
        else
        	n = Node_Hash.new(contact,@head)
            @head = n
            
        end
    end

     def remove_rec(prev, apellido)
        if prev.next_node.nil?
            return "Ya esta eliminado"
        end
        if prev.next_node.contact.surname == apellido
            prev.next_node = prev.next_node.next_node
            true
        else
        	remove_rec(prev.next_node, apellido)
        end
    end

    def remove apellido
        if @head.contact.surname == apellido
            @head = @head.next_node
            true
        else
            remove_rec(@head, apellido)
        end
    end

    def show_all contact
    	puts "Nombre : #{contact.name}"
    	puts "Apellido : #{contact.surname}"
    	puts "Telefono : #{contact.phone}"
    	puts "E-mail : #{contact.address}"
    end


    def search apellido
        n = @head
        while !n.nil? && n.contact.surname != apellido
            n = n.next_node
        end

        if n == nil
        	puts "El contacto buscado no se encuentra"
        else
            show_all(n.contact)
        end
    end
    
end
   


class Hash_Phonebook
    attr_accessor :size , :table
    def initialize size
        @size = size
        @table = Array.new(size) {Hash_List.new}
    end
    
	def Hash key
		value = 0
		j = 0
		until j == key.length
			value += key[j].downcase.ord
			j += 1
		end
		value % @size
	end

    def insert_hash contact
        key = Hash(contact.surname)
        @table[key].insert(contact)
    end

    def remove_hash apellido 
    	key = Hash(apellido)
    	@table[key].remove(apellido)
    	puts "Eliminado con exito!"
    end

    def search_hash apellido
    	key = Hash(apellido)
    	table[key].search(apellido)
    end
end

p = Hash_Phonebook.new(24)

c1 = Contact.new("Gonzalo","Ramos","90700741","oli@gmail.com")
c2 = Contact.new("Rosamel","Ramirez","66666666","tumadre@gmail.com")
c3 = Contact.new("Armando","Barrera","12345678","soyweco@gmail.com")
c4 = Contact.new("Benito","Camelo","00000000","soyimbecil@gmail.com")

p.insert_hash(c1)
p.insert_hash(c2)
p.insert_hash(c3)
p.insert_hash(c4)

p.search_hash("Ramirez")

p.remove_hash("Ramirez")

p.search_hash("Ramirez")

p.search_hash("Camelo")



