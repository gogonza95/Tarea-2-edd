puts "Bienvenido a tu Libreta de Contactos!"
puts "¿Que diccionario deseas utilizar?"
puts "1-AVL"
puts "----------------------------------"
puts "2-Tabla de Hash"

op = gets.chomp

if op == "1"
	load 'AVL.rb'
	avl = AVL_Phonebook.new()
	i = true
	while (i)
	puts "¿Que deseas hacer?"
	puts "---------------------------"
	puts "1-Agregar"
	puts "2-Editar"
	puts "3-Eliminar"
	puts "4-Salir"
	if opcion == "1"
		puts "---------------------------"
		puts "Inserte los datos correspondientes"
		puts "Nombre :"
		name = gets.chomp
		puts "Apellido :"
		surname = gets.chomp
		puts "Telefono :"
		phone = gets.chomp
		puts "E-mail :"
		email = gets.chomp
		c1 = Contact.new(name,surname,phone,email)
		avl.insert(c1)
		puts "Contacto agregado exitosamente!"
	elsif opcion == "2"
		puts "---------------------------"
		puts "Opcion no disponible"
	elsif opcion == "3"
		puts "---------------------------"
		puts "Opcion no disponible"
	elsif opcion == "4"
		i = false
	end
elsif op == "2"
	load 'HASH.rb'
	hash = Hash_Phonebook.new(29)
	i = true
	while(i)
	puts "¿Que deseas hacer?"
	puts "---------------------------"
	puts "1-Agregar"
	puts "2-Editar"
	puts "3-Eliminar"
	puts "4-Salir"
	opcion = gets.chomp
	if opcion == "1"
		puts "---------------------------"
		puts "Inserte los datos correspondientes"
		puts "Nombre :"
		name = gets.chomp
		puts "Apellido :"
		surname = gets.chomp
		puts "Telefono :"
		phone = gets.chomp
		puts "E-mail :"
		email = gets.chomp
		c1 = Contact.new(name,surname,phone,email)
		hash.insert_hash(c1)
		puts "Contacto agregado exitosamente!"
	elsif opcion == "2"
		puts "---------------------------"
		puts "Inserte el apellido del contacto que desea editar:"
		surname1 = gets.chomp
		puts "Inserte los nuevos datos del contacto"
		puts "Nombre :"
		name = gets.chomp
		puts "Apellido :"
		surname2 = gets.chomp
		puts "Telefono :"
		phone = gets.chomp
		puts "E-mail :"
		email = gets.chomp
		c2 = Contact.new(name,surname2,phone,email)
		hash.edit_hash(surname1,c2)
		puts "Contacto editado exitosamente!"
	elsif opcion == "3"
		puts "---------------------------"
		puts "Inserte el apellido del contacto que desea eliminar"
		surname3 = gets.chomp
		hash.remove_hash(surname3)
		puts "Contacto eliminado con exito!"
	elsif opcion == "4"
		i = false
	end
end
end







